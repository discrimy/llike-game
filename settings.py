SAVE_EXTENSION = '.llgsave'
SAVE_FORMAT = ('L Like Game Сохранения', '*' + SAVE_EXTENSION)

ABOUT_TITLE = 'L Like Game'
ABOUT_MESSAGE = """L Like Game

Быстрые клавиши:
Ctrl-Z - отмена последнего хода
Ctrl-R - перезагрузка игры

Создано @discrimy
Исходный код: https://gitlab.com/discrimy/llike-game
"""
