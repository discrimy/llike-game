from abc import ABC, abstractmethod
from collections import OrderedDict
from copy import deepcopy
from dataclasses import dataclass
from enum import Enum
from typing import List, Optional, Tuple, Generator, Set

from game.exceptions import FieldError


class CellState(Enum):
    EMPTY = '0'
    MARKED = '*'
    PLAYER = 'P'
    AVAILABLE = '+'


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, other: 'Point') -> 'Point':
        return Point(self.x + other.x, self.y + other.y)


class GameField:
    columns: int
    rows: int
    player_pos: Optional[Point]
    marked_points: Set[Point]

    history: List['MovePlayerCommand']

    def __init__(self, columns: int, rows: int):
        self.rows = rows
        self.columns = columns
        self.player_pos = None
        self.marked_points = set()
        self.history = []

    def get_available_cells(self, from_point: Point) -> Set[Point]:
        return {
            point for point in (
                from_point + Point(-1, -2), from_point + Point(1, -2),
                from_point + Point(-1, 2), from_point + Point(1, 2),
                from_point + Point(-2, -1), from_point + Point(-2, 1),
                from_point + Point(2, -1), from_point + Point(2, 1),
            ) if self._check_size(point) and point not in self.marked_points
        }

    @property
    def available_cells(self) -> Set[Point]:
        return self.get_available_cells(from_point=self.player_pos)

    def __getitem__(self, point: Point) -> CellState:
        self._assert_size(point)

        if point in self.marked_points:
            return CellState.MARKED
        if point == self.player_pos:
            return CellState.PLAYER
        if self.player_pos is not None and point in self.available_cells:
            return CellState.AVAILABLE
        return CellState.EMPTY

    def cells(self) -> Generator[Tuple[Point, CellState], None, None]:
        for x in range(self.columns):
            for y in range(self.rows):
                point = Point(x, y)
                yield point, self[point]

    def apply_command(self, command: 'MovePlayerCommand'):
        command.apply(self)
        self.history.append(command)

    def undo_last_command(self) -> bool:
        try:
            command = self.history.pop()
        except IndexError:
            return False
        else:
            command.undo(self)
            return True

    def create_snapshot(self) -> 'GameFieldSnapshot':
        return GameFieldSnapshot(
            columns=self.columns,
            rows=self.rows,
            player_pos=self.player_pos,
            marked_points=self.marked_points.copy(),
            history=deepcopy(self.history),
        )

    @classmethod
    def restore_from_snapshot(cls, snapshot: 'GameFieldSnapshot') -> 'GameField':
        game_field = GameField(
            columns=snapshot.columns,
            rows=snapshot.rows,
        )
        game_field.player_pos = snapshot.player_pos
        game_field.marked_points = set(snapshot.marked_points)
        game_field.history = deepcopy(snapshot.history)
        return game_field

    def _check_size(self, point: Point) -> bool:
        if point.x < 0 or point.x >= self.columns:
            return False
        if point.y < 0 or point.y >= self.rows:
            return False
        return True

    def _assert_size(self, point: Point):
        if not self._check_size(point):
            raise IndexError(f'Field size is {self.columns} x {self.rows}, but required cell is {point}')


class MovePlayerCommand:
    from_point: Optional[Point]

    def __init__(self, to_point: Point):
        self.to_point = to_point

        self.from_point = None

    def apply(self, field: GameField):
        self._assert_new_player_pos(field)

        self.from_point = field.player_pos

        if field.player_pos is not None:
            field.marked_points.add(field.player_pos)
        field.player_pos = self.to_point

    def _assert_new_player_pos(self, field: GameField):
        if self.to_point in field.marked_points:
            raise FieldError(f'Point {self.to_point} is already marked')
        if field.player_pos is not None:
            if self.to_point not in field.available_cells:
                raise FieldError(f'Point {self.to_point} not in available one')

    def undo(self, field: GameField):
        if self.from_point is not None:
            field.marked_points.remove(self.from_point)
        field.player_pos = self.from_point


@dataclass(frozen=True)
class GameFieldSnapshot:
    columns: int
    rows: int
    player_pos: Optional[Point]
    marked_points: Set[Point]

    history: List['MovePlayerCommand']
