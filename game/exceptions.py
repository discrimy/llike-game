class LLikeGameError(Exception):
    pass


class FieldError(LLikeGameError):
    pass
