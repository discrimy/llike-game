import pickle
import tkinter as tk
import tkinter.filedialog as fd
from functools import partial
from tkinter import messagebox
from typing import Dict, Optional

import settings
from game.exceptions import FieldError
from game.models import GameField, Point, CellState, MovePlayerCommand, GameFieldSnapshot
from settings import SAVE_FORMAT, SAVE_EXTENSION

RESTART_COMBINATION = '<Control-r>'
UNDO_COMBINATION = '<Control-z>'

PLAYER_CELL_COLOR = 'orange'
AVAILABLE_CELL_COLOR = 'yellow'
MARKED_CELL_COLOR = 'red'
EMPTY_CELL_COLOR = 'white'


class LLikeGame(tk.Frame):
    field: GameField

    btns: Dict[Point, tk.Button]

    def __init__(self, parent: tk.Tk, field: GameField):
        super().__init__(parent)
        self.parent = parent
        self.field = field

        self.label = tk.Label(self)
        self.label.grid(column=0, row=0, columnspan=self.field.columns)
        self.btns = {}
        for point, _ in self.field.cells():
            btn_frame = tk.Frame(self, width=30, height=30)
            btn = tk.Button(
                btn_frame,
                command=partial(self._on_button_pressed, point=point)
            )
            btn_frame.rowconfigure(0, weight=1)
            btn_frame.columnconfigure(0, weight=1)
            btn_frame.grid_propagate(0)
            btn_frame.grid(column=point.x, row=point.y + 1)
            btn.grid(sticky="NWSE")
            self.btns[point] = btn

        self.draw()

    def undo_last_command(self):
        self.field.undo_last_command()
        self.draw()

    def _on_button_pressed(self, point: Point):
        try:
            self.field.apply_command(MovePlayerCommand(to_point=point))
        except FieldError:
            pass
        self.draw()

    def draw(self):
        for point, cell_state in self.field.cells():
            btn = self.btns[point]
            if cell_state == CellState.EMPTY:
                btn.configure(bg=EMPTY_CELL_COLOR)
            elif cell_state == CellState.MARKED:
                btn.configure(bg=MARKED_CELL_COLOR)
            elif cell_state == CellState.AVAILABLE:
                btn.configure(bg=AVAILABLE_CELL_COLOR)
            elif cell_state == CellState.PLAYER:
                btn.configure(bg=PLAYER_CELL_COLOR)
        filled_count = sum(
            1 for _, cell_state in self.field.cells()
            if cell_state in (CellState.MARKED, CellState.PLAYER)
        )
        all_count = self.field.columns * self.field.rows
        self.label['text'] = f'Заполнено {filled_count}/{all_count}'


class Application:
    l_like_game: Optional[LLikeGame]
    root: tk.Tk

    def __init__(self, title: str, favicon: str):
        self.root = tk.Tk()
        self.root.title(title)
        self.root.iconbitmap(favicon)

        menu = tk.Menu(self.root)
        menu.add_command(label='Сохранить', command=self.on_game_save)
        menu.add_command(label='Загрузить', command=self.on_game_load)
        menu.add_command(label='Об игре', command=partial(
            messagebox.showinfo, title=settings.ABOUT_TITLE, message=settings.ABOUT_MESSAGE
        ))
        self.root.config(menu=menu)

        self.l_like_game = None

        self._init_form_gui()

        self.root.bind_all('<Key>', self._on_key_pressed)

    def _on_key_pressed(self, event):
        if event.state & 4 > 0:
            if chr(event.keycode) == 'R':
                return self._on_restart()
            if chr(event.keycode) == 'Z':
                return self._on_undo()

    def _on_undo(self):
        if self.l_like_game is None:
            return
        self.l_like_game.undo_last_command()

    def on_game_save(self):
        if self.l_like_game is None:
            return
        snapshot = self.l_like_game.field.create_snapshot()
        filename = fd.asksaveasfilename(
            title='Файл для сохранения:',
            filetypes=[SAVE_FORMAT],
            defaultextension=SAVE_EXTENSION
        )
        if not filename:
            return
        with open(filename, mode='wb') as file:
            pickle.dump(snapshot, file=file)

    def on_game_load(self):
        filename = fd.askopenfilename(
            title='Файл для сохранения:',
            filetypes=[SAVE_FORMAT],
        )
        if not filename:
            return
        with open(filename, mode='rb') as file:
            snapshot: GameFieldSnapshot = pickle.load(file)
        game_field = GameField.restore_from_snapshot(snapshot)
        self.start_game(game_field=game_field)

    def _on_restart(self):
        if self.l_like_game is None:
            return
        self.start_game(game_field=GameField(
            columns=self.l_like_game.field.columns,
            rows=self.l_like_game.field.rows,
        ))

    def _init_form_gui(self):
        form_frame = tk.Frame(self.root)
        label_columns = tk.Label(form_frame, text='Столбцов')
        label_columns.pack(side=tk.LEFT)
        self.entry_columns = tk.Entry(form_frame)
        self.entry_columns.pack(side=tk.LEFT)
        label_rows = tk.Label(form_frame, text='Строк')
        label_rows.pack(side=tk.LEFT)
        self.entry_rows = tk.Entry(form_frame)
        self.entry_rows.pack(side=tk.LEFT)
        start_btn = tk.Button(
            form_frame,
            text='Старт',
            command=self._on_start_game,
        )
        start_btn.pack(side=tk.LEFT)
        form_frame.pack()

    def _on_start_game(self):
        try:
            columns = int(self.entry_columns.get())
            rows = int(self.entry_rows.get())
        except ValueError:
            messagebox.showinfo(title='Ошибка', message='Введите число!')
            self.root.focus_force()
            return
        else:
            self.start_game(game_field=GameField(columns=columns, rows=rows))

    def start_game(self, game_field: GameField):
        if self.l_like_game is not None:
            self.l_like_game.destroy()
        self.l_like_game = LLikeGame(parent=self.root, field=game_field)
        self.l_like_game.pack()

    def start(self):
        self.root.mainloop()
