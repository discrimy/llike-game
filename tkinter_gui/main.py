import os
import sys

from game.models import GameField
from tkinter_gui.gui import Application


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except AttributeError:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


if __name__ == '__main__':

    application = Application(
        title='L Like Game',
        favicon=resource_path('favicon.ico'),
    )
    application.start_game(game_field=GameField(columns=5, rows=5))
    application.start()
